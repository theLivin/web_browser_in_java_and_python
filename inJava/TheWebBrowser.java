import javax.swing.JFrame;

public class TheWebBrowser{
    public static void main(String []args){
        // Create ReadPage object
        ReadPage browser = new ReadPage();

        // Exit On Close
        browser.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}