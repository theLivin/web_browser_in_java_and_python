import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class ReadPage extends JFrame{
    // Address Bar
    private JTextField addressBar;
    // Display Window
    private JEditorPane display;

    // Constructor
    public ReadPage(){
        // Browser Title Bar
        super("theLivinBrowser 1.0");

        // Address Bar Text Field Default Content
        addressBar = new JTextField("http://www.google.com");
        
        // Add 'enter' Event Listener To Address Bar
        addressBar.addActionListener(
            /// Anonymous Inner Class
            new ActionListener(){
                /// 'enter' listener
                public void actionPerformed(ActionEvent event){
                    /// what should happen when user presses enter
                    loadWebPage(event.getActionCommand());      /// get string in address bar and pass it into loadWebPage()
                }
            }
        );

        // Add Address Bar To Broser Frame
        add(addressBar, BorderLayout.NORTH);

        // Create display Window
        display = new JEditorPane();

        // Refuse user from editing page content
        display.setEditable(false);

        // Create Listener For Links On WebPage
        display.addHyperlinkListener(
            // Anonymous Inner Class
            new HyperlinkListener(){
                /// wait for event to happen on link
                public void hyperlinkUpdate(HyperlinkEvent event){
                    /// if user clicks on link,
                    if(event.getEventType()==HyperlinkEvent.EventType.ACTIVATED){
                        /// redirect to clicked link
                        loadWebPage(event.getURL().toString());
                    }
                }
            }
        );

        // Add display to screen/jframe
        add(new JScrollPane(display), BorderLayout.CENTER);

        // Set Default Window/Frame Size
        setSize(500,300);

        // Make Fram Visible
        setVisible(true);

    }

    // Load Web Page Content of user_url
    private void loadWebPage(String user_url){
        try {
            // GoTo url
            display.setPage(user_url);
            System.out.println("Loading...");

            // Keep addressBar content
            addressBar.setText(user_url);
        } catch (Exception e) {
            System.out.println("Website cannot be reached!\nPrefix address with http://");
        }
    }
}